package com.mat.shopping.basket;

import com.mat.shopping.calculators.BasketItemCalculator;

import java.util.Map;

/**
 * Implements a shopping basket that can handle a list of items, and calculate the total cost. This is achieved
 * using a map of calculators.
 * A calculators maps an item name to a specific calculator that is able to calculate the cost of a number of items
 * of the same type.
 */
public class ShoppingBasketImpl implements ShoppingBasket {

    private final Map<String, BasketItemCalculator> calculators;

    public ShoppingBasketImpl(Map<String, BasketItemCalculator> calculators) {
        this.calculators = calculators;
    }

    /**
     * Calculates the total cost of all the items using calculators.
     *
     * @param items represents the list of items in the shopping basket.
     * @throws UnsupportedOperationException if none of the calculators support the item.
     * @return total cost of each item in the basket while considering the price rules.
     */
    @Override
    public int calculateTotalCost(String[] items) {
        if (items == null || items.length == 0) {
            return 0;
        }

        for (String item : items) {
            if (!calculators.containsKey(item)) {
                throw new UnsupportedOperationException(item + " not supported by the basket");
            }
            calculators.get(item).add();
        }

        return calculators.values().stream().mapToInt(BasketItemCalculator::calculateCost).sum();
    }

}
