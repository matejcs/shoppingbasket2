package com.mat.shopping.basket;

public interface ShoppingBasket {


    /**
     * Calculates the total cost of the shopping items in the shopping basket.
     * @param items represents the list of items in the shopping basket.
     * @throws UnsupportedOperationException if the basket encounters an unsupported item.
     * @return total cost of each of the items in the basket. 0 if no items specified.
     */
    int calculateTotalCost(String[] items);

}
