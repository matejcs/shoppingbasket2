package com.mat.shopping.basket;

import com.mat.shopping.calculators.*;

import java.util.Map;

/**
 * Builds a shopping basket supporting a number of different items.
 */
public class ShoppingBasketBuilder {

    public static ShoppingBasket buildSimpleShoppingBasket() {
        Map<String, BasketItemCalculator> calculators = Map.ofEntries(
                getEntry(new SimpleCalculator("Apple", 35)),
                getEntry(new SimpleCalculator("Banana", 20)),
                getEntry(new ThreeForThePriceOfTwoCalculator("Lime", 15)),
                getEntry(new BuyOneGetOneFreeCalculator("Melon", 50))
        );
        return new ShoppingBasketImpl(calculators);
    }

    private static Map.Entry<String, BasketItemCalculator> getEntry(AbstractCalculator itemCalculator) {
        return Map.entry(itemCalculator.getName(), itemCalculator);
    }

}
