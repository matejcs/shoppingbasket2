package com.mat.shopping.calculators;

public interface BasketItemCalculator {

    /**
     * Adds a new item of the same type into the item basket.
     */
    void add();

    /**
     * Calculates the total cost of the items in the basket.
     * @return the total cost of items in the basket.
     */
    int calculateCost();

}
