package com.mat.shopping.calculators;

/**
 * Implements a basket calculation with an "buy one get one free" offer.
 */
public class BuyOneGetOneFreeCalculator extends AbstractCalculator {

    /**
     * A calculator needs to have the item name (representing its type) and cost specified.
     *
     * @param itemName the name of the item calculated by the calculator
     * @param cost     the cost of a single item
     */
    public BuyOneGetOneFreeCalculator(String itemName, int cost) {
        super(itemName, cost);
    }

    @Override
    public int calculateCost() {
        int totalCost = (this.count / 2) * this.cost;
        if (this.count % 2 == 1) {
            totalCost += this.cost;
        }
        return totalCost;
    }
}
