package com.mat.shopping.calculators;

/**
 * Implements methods shared by all specific calculators.
 */
public abstract class AbstractCalculator implements BasketItemCalculator {

    private final String itemName;

    protected final int cost;

    protected int count;

    /**
     * A calculator needs to have the item name (representing its type) and cost specified.
     * @param itemName the name of the item calculated by the calculator
     * @param cost the cost of a single item
     */
    public AbstractCalculator(String itemName, int cost) {
        this.itemName = itemName;
        this.cost = cost;
    }

    /**
     * Increases the count of items in the calculator basket by one.
     */
    public void add() {
        ++this.count;
    }

    public String getName() {
        return itemName;
    }
}
