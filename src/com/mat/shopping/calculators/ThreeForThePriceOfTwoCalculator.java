package com.mat.shopping.calculators;

/**
 * Implements a basket calculation with an "three for the price of two" offer.
 */
public class ThreeForThePriceOfTwoCalculator extends AbstractCalculator {

    /**
     * A calculator needs to have the item name (representing its type) and cost specified.
     *
     * @param itemName the name of the item calculated by the calculator
     * @param cost     the cost of a single item
     */
    public ThreeForThePriceOfTwoCalculator(String itemName, int cost) {
        super(itemName, cost);
    }

    @Override
    public int calculateCost() {
        int totalCost = (this.count / 3) * (this.cost * 2);
        if (this.count % 3 != 0) {
            totalCost += (this.count % 3) * this.cost;
        }
        return totalCost;
    }
}
