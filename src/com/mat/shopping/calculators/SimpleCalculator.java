package com.mat.shopping.calculators;

/**
 * Implements the simplest form of cost calculation, where the cost matches the number of items multiplied by the price
 * of a single item.
 */
public class SimpleCalculator extends AbstractCalculator {

    /**
     * A calculator needs to have the item name (representing its type) and cost specified.
     *
     * @param itemName the name of the item calculated by the calculator
     * @param cost     the cost of a single item
     */
    public SimpleCalculator(String itemName, int cost) {
        super(itemName, cost);
    }

    @Override
    public int calculateCost() {
        return this.count * this.cost;
    }
}
